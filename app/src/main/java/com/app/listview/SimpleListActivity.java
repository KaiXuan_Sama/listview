package com.app.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SimpleListActivity extends AppCompatActivity {
    private ListView lv;

    // 创建一个用于桥接的构造器
    private SimpleAdapter SimpleAdapter;

    // 创建用于保存数据的List
    private List<Map<String,Object>> mList = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_list);

        lv = findViewById(R.id.lv);

        initData();
        initView();
    }

    private void initView() {
        // 创建构造器
        SimpleAdapter = new SimpleAdapter(
                this,                                           /*上下文*/
                mList,                                                  /*使用的map键值对*/
                R.layout.list_item_layout,                              /*使用的布局文件（自定义）*/
                new String[]{"img","title","content"},                  /*键（与下一一对应）*/
                new int[]{R.id.iv_img,R.id.tv_title,R.id.tv_content}    /*值（与上一一对应）*/
        );

        // 链接构造器
        lv.setAdapter(SimpleAdapter);

        // 设置点击事件
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(SimpleListActivity.this,"你点击了" + i + "条目",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initData() {
        for (int i = 0; i < 50; i++) {
            Map<String,Object> map = new HashMap();
            map.put("img",R.drawable.img);  // 循环50次存同一个图片数据
            map.put("title","这是标题" + i);
            map.put("content","这是内容" + i);
            mList.add(map);
        }
    }
}