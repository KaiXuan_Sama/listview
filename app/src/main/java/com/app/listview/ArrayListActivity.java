package com.app.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

public class ArrayListActivity extends AppCompatActivity {
    private ListView lv;

    // 定义用于储存String信息的List对象
    private List<String> StringList = new LinkedList<>();

    // 构造一个用于桥接的构造器
    private ArrayAdapter<String> ArrayAdapter;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_array_list);

        lv = findViewById(R.id.lv);

        initData(); // 数据初始化
        initView(); // 布局初始化
    }

    private void initView() {
        // 新建构造器对象，并初始化上下文、列表所使用的布局样式、布局内使用的信息列表
        ArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,StringList);

        // 将布局和构造器进行桥接
        lv.setAdapter(ArrayAdapter);

        // 设置条目的点击事件
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(ArrayListActivity.this,"你点击了" + i + "条目",Toast.LENGTH_SHORT).show();
            }
        });

        // 设置条目的长按事件
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(ArrayListActivity.this,"你长按了" + i + "条目",Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }

    /**
     * 数据初始化
     */
    private void initData() {
        for (int i = 0; i < 50; i++) {
            StringList.add("这是第" + i + "个条目");
        }
    }
}