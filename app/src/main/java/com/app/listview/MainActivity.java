package com.app.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void toArrayList(View view) {
        Intent intent = new Intent(this,ArrayListActivity.class);
        startActivity(intent);
    }

    public void toSimpleList(View view) {
        Intent intent = new Intent(this,SimpleListActivity.class);
        startActivity(intent);
    }
}